
library(data.table)
library(moments)
library(fpc)

load('Final Report/mydat_corrected_imputed.RData')

#### Define functions ####

num.outliers <- function(x){ # using "3 sigma rule"
  m <- mean(x)
  s <- sd(x)
  l <- (m-(3*s))
  u <- (m+(3*s))
  n <- sum(!x %between% c(l, u))
  return(n)
}

log.trans <- function(x){
  if(min(x, na.rm=T)>=1){
    y <- log(x)
  }else{
    myconstant <- abs(min(x, na.rm=T))+1
    xnew <- x+myconstant
    y <- log(xnew)
  }
  return(y)
}

#### Define variables to be transformed ####

# doesn't make sense to transform categorical variables
numvars <- names(mydat_corrected_imputed)[sapply(mydat_corrected_imputed, class) == 'numeric']
# exclude imputation indicators
imputation_indicators <- grep('I_', names(mydat_corrected_imputed), value=T)
transvars <- setdiff(numvars, imputation_indicators)
# exclude Customer_ID and churn
transvars <- setdiff(transvars, c('Customer_ID', 'churn'))

#### Transformations ####

# preallocate a data frame to report the transformation used on each variable
trans_used <- data.table(Variable=transvars, 
                         OutliersBefore=as.character(NA), SkewBefore=as.numeric(NA), 
                         Transformation=as.character(NA), 
                         OutliersAfter=as.character(NA), SkewAfter=as.numeric(NA))

trans.func <- function(varname, x){
  
  # calculate percent outliers before transformation using "3 sigma rule"
  outliers.before <- 100*num.outliers(x)/length(x)
  
  # calculate skewness before transformation
  orig.skw <- skewness(x, na.rm=T) 
  
  if(abs(orig.skw)<=0.2){ # -0.2 < skewness < 0.2 is acceptable
    transform.used <- 'None Needed'
    y <- x
    new.skw <- orig.skw
    
  }else if(orig.skw>0.2){ # first try Log Transformation on positive skew variables
    transform.used <- 'Log Transformation'
    y <- log.trans(x)
    new.skw <- skewness(y, na.rm=T)
    
  }else if(orig.skw<(-0.2)){ # first try Square Transformation on negative skew variables
    transform.used <- 'Square Transformation'
    y <- (x^2)
    new.skw <- skewness(y, na.rm=T)
    
  }
  # still skewed? use Rank Transformation
  if(abs(new.skw)>0.2){ 
    transform.used <- 'Rank Transformation'
    y <- rank(x, na.last='keep')
    new.skw <- skewness(y, na.rm=T)
  }
  # last resort is Bucket Transformation aka binning
  if(abs(new.skw)>0.2){ 
    transform.used <- 'Bucket Transformation'
    # sing pamk function from fpc package chooses optimal number of clusters
    y <- as.character(pamk(x, usepam=F)$pamobject$clustering)
    new.skw <- as.numeric(NA)
  }
  
  # calculate percent outliers after transformation using "3 sigma rule"
  if(transform.used!='Bucket Transformation'){
    outliers.after <- 100*num.outliers(y)/length(y)
  }else{
    outliers.after <- as.numeric(NA)
  }
  
  # populate trans_used data frame with information about transformation
  trans_used[Variable==varname, OutliersBefore:=paste0(signif(outliers.before, 3), '%')]
  trans_used[Variable==varname, SkewBefore:=orig.skw]
  trans_used[Variable==varname, Transformation:=transform.used]
  trans_used[Variable==varname, OutliersAfter:=paste0(signif(outliers.after, 3), '%')]
  trans_used[Variable==varname, SkewAfter:=new.skw]
  
  # return transformed data vector
  return(y)
}

# apply the function defined above in parallel
myresults <- mapply(trans.func, varname=transvars, x=mydat_corrected_imputed[, transvars, with=F], SIMPLIFY=F)

# convert to data table (is a list currently)
myresults.df <- as.data.table(myresults)

# replace original variables with transformed variables
mydat_corrected_imputed_transformed <- cbind(mydat_corrected_imputed[, !transvars, with=F], myresults.df)

# save summary table
save(trans_used, file='Final Report/Transformations.RData')

# save data
save(mydat_corrected_imputed_transformed, file='Final Report/mydat_corrected_imputed_transformed.RData')
