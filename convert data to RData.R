# Convert SAS data files to .RData

# make sure you're in the project folder
getwd()

library(sas7bdat) # library for reading .sas7bdat files into R
library(data.table)

# load training data
mydat_train <- read.sas7bdat('Data/dpfinal.sas7bdat')
# load test data
mydat_test <- read.sas7bdat('Data/dptest.sas7bdat')

## convert factors to character
i <- sapply(mydat_train, is.factor)
mydat_train[i] <- lapply(mydat_train[i], as.character)

i <- sapply(mydat_test, is.factor)
mydat_test[i] <- lapply(mydat_test[i], as.character)

# rbind the two together
mydat_test$churn <- as.character(NA)
mydat <- rbind(mydat_test, mydat_train)

# convert churn to class factor
mydat$churn <- factor(mydat$churn)

# create indicator for training/test
mydat$dataset <- as.character(NA)
mydat$dataset[!is.na(mydat$churn)] <- 'Train'
mydat$dataset[is.na(mydat$churn)] <- 'Test'

mydat <- as.data.table(mydat) # convert to class data.table

# save the data for analysis
save(mydat, file='Final Report/dpfinal.RData')
